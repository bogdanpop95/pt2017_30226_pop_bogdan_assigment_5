package cerinta1;
import java.util.*;
import java.util.stream.Stream;

import monitoredData.MonitoredData;

public class Cerinta1{
	private List<MonitoredData> md;
	public static long count;
	
	public Cerinta1(List<MonitoredData> md) {
		this.md = md;
	}
	
	public void print() {
		long count1, count2;
		count1 = md.stream().map(t -> t.getDay(t.getStartTime())).distinct().count();
		count2 = md.stream().map(t -> t.getDay(t.getEndTime())).distinct().count();
		count = max(count1, count2);
		System.out.println("Numarul de zile distincte : " + count);
	}

	private long max(long count1, long count2) {
		return count1 > count2 ? count1 : count2;
	}
}
