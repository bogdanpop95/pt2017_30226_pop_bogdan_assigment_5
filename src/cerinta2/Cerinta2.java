package cerinta2;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import monitoredData.MonitoredData;

public class Cerinta2 {
	private List<MonitoredData> md;
	
	public Cerinta2(List<MonitoredData> md) {
		this.md = md;
	}
	
	public Map<String, Long> generate() {
		Map<String, Long> mapped = new HashMap<String, Long>();
		mapped = md.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));
		return mapped;
	}
	
	public void print() {
		generate().forEach((k, v) -> {
			System.out.println(k + " : " + v);
		});
	}
	
	public void writeInFile() {
		try {
			PrintWriter writer = new PrintWriter("cerinta2.txt", "UTF-8");
			writer.flush();
			writer.println("\t Activity  \t  |  \t  Count ");
			generate().forEach((k, v) -> {
				writer.println("\t " + k + "  \t  |  \t  " + v);
			});
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
