package cerinta4;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import monitoredData.MonitoredData;

public class Cerinta4 {
	private List<MonitoredData> md;
	
	public Cerinta4(List<MonitoredData> md) {
		this.md = md;
	}
	
	public Map<String, Long> generate() {
		Map<String, Long> mapped = new HashMap<String, Long>();
		mapped = md.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel, 
				Collectors.summingLong(MonitoredData::getDuration)));
		return mapped;
	}
	
	public void print() {
		generate().forEach((k, v) -> {
			if (v >= (10 * 60 * 60 * 1000))
				System.out.println(k + " : " + convert(v));
				
		});
	}
	
	public void writeInFile() {
		try {
			PrintWriter writer = new PrintWriter("cerinta4.txt", "UTF-8");
			writer.flush();
			writer.println("\t Activity  \t  |  \t  Duration ");
			generate().forEach((k, v) -> {
				if (v >= (10 * 60 * 60 * 1000))
					writer.println("\t " + k + "  \t  |  \t  " + convert(v));
					
			});
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String convert(Long time) {
		long temp;
		StringBuilder sb = new StringBuilder();
		sb.append(time.longValue() / ( 1000 * 60 * 60 ) + ":");
		temp = (time.longValue() / (1000 * 60)) % 60;
		sb.append((temp < 10 ? ("0" + temp) : temp) + ":");
		temp = (time.longValue() / 1000) % 60;
		sb.append((temp < 10 ? ("0" + temp) : temp));
		return sb.toString();
	}
}
