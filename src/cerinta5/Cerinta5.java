package cerinta5;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import monitoredData.MonitoredData;

public class Cerinta5 {
	
	private List<MonitoredData> md;
	
	public Cerinta5(List<MonitoredData> md) {
		this.md = md;
	}
	
	public List<String> generate() {
		List<String> result = new ArrayList<String>();
		result = md.stream().filter( f -> f.getDuration() < (5 * 60 * 1000)).collect(
				Collectors.groupingBy(act -> act.getActivityLabel(), Collectors.counting())).entrySet()
				.stream().filter( succ -> succ.getValue() > 0.9 * 
						md.stream().filter(act -> act.getActivityLabel().equals(succ.getKey())).count()).
				map(e -> e.getKey()).collect(Collectors.toList());
				
		return result;		
	}
	
	public void print() {
		generate().forEach(str -> {
			System.out.println(str);
		});
	}
	
	public void writeInFile() {
		try {
			PrintWriter writer = new PrintWriter("cerinta5.txt", "UTF-8");
			writer.flush();
			writer.println("\t Activity  \t  |  \t  Occurences < 5 min ");
			generate().forEach(str -> {
				writer.println(str);
			});
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
