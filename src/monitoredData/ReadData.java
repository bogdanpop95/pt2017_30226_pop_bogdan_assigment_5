package monitoredData;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
public class ReadData {
	private List<MonitoredData> monitoredData = new ArrayList<MonitoredData>();
	
	public void read() {
		try (Stream<String> stream = Files.lines(Paths.get("./Activities.txt"))) {
			monitoredData = stream.map(info -> generate(info)).collect(Collectors.toList());
			System.out.println("Read complete! Number of lines: " + monitoredData.size());
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	
	public MonitoredData generate(String line) {
		String[] result;
		result = line.split("\t\t", 3);
		result[2] = result[2].split("\t")[0];
		MonitoredData md = new MonitoredData(result[0], result[1], result[2]);
		return md;
	}
	
	public List<MonitoredData> getMonitoredData() {
		return monitoredData;
	}
	
	public void printData () {
		monitoredData.forEach( m -> {
			System.out.println(m);
		});
	}
}
