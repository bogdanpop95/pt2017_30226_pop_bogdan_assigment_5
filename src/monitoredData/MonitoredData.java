package monitoredData;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
public class MonitoredData implements Comparable<MonitoredData>{
	
	private Date startTime;
	private Date endTime;
	private String activityLabel;
	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static Map<Integer, Integer> days = new HashMap<Integer, Integer>();
	private static int dayCount;
	
	public MonitoredData(String startTime, String endTime, String activityLabel) {
		dayCount = 1;
		try {
			this.startTime = ft.parse(startTime);
			this.endTime = ft.parse(endTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.activityLabel = activityLabel;
	}

	public Date getStartTime() {
		return startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public String getActivityLabel() {
		return activityLabel;
	}
	
	public String toString() {
		return startTime + "\t\t" + endTime + "\t\t" + activityLabel; 
	}
	
	public Date getDay(Date time) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(time);
		
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		return calendar.getTime();
	}
	
	public Integer getD() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(this.startTime);
		
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		if (days.containsKey(calendar.getTime().hashCode()))
			;
		else {
			days.put(calendar.getTime().hashCode(), dayCount);
			dayCount++;
		}
		return days.get(calendar.getTime().hashCode());
	}
	
	public int compareTo(MonitoredData obj) {
		if (this.getDay(this.startTime).after(obj.getDay(obj.getStartTime())))
			return 1;
		else if (this.getDay(this.startTime).before(obj.getDay(obj.getStartTime())))
			return -1;
		else
			return 0;
	}
	
	public Long getDuration() {
		return this.endTime.getTime() - this.startTime.getTime();
	}
}
