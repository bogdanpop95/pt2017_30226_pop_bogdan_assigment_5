package cerinta3;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import monitoredData.MonitoredData;

public class Cerinta3 {
	private List<MonitoredData> md;
	
	public Cerinta3(List<MonitoredData> md) {
		this.md = md;
	}
	
	public Map<Integer, Map<String, Long>> generate() {
		Map<Integer, Map<String, Long>>  mapped = new HashMap<Integer, Map<String, Long>>();
		//md.sort(null);
		mapped = md.stream().sequential().collect(Collectors.groupingByConcurrent
				(MonitoredData::getD, Collectors.groupingBy(
						MonitoredData::getActivityLabel, Collectors.counting())));
		return mapped;
	}
	
	public void print() {
		generate().forEach((k, v)-> {
			System.out.println("--Day " + k + ":");
			v.forEach((k2, v2) -> {
				System.out.println(k2 + " : " + v2);
			});
			System.out.println();
		});
	}
	
	public void writeInFile() {
		try {
			PrintWriter writer = new PrintWriter("cerinta3.txt", "UTF-8");
			writer.flush();
			generate().forEach((k, v)-> {
				writer.println("--Day " + k + ":");
				v.forEach((k2, v2) -> {
					writer.println(k2 + " : " + v2);
				});
				writer.println();
			});
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
