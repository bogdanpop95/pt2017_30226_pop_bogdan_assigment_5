package start;
import monitoredData.*;
import cerinta1.Cerinta1;
import cerinta2.Cerinta2;
import cerinta3.Cerinta3;
import cerinta4.Cerinta4;
import cerinta5.Cerinta5;

public class Start {
	public static void main (String[] args) {
		ReadData rd = new ReadData();
		rd.read();
		rd.printData();
		System.out.println("----Cerinta 1: ----");
		Cerinta1 c1 = new Cerinta1(rd.getMonitoredData());
		c1.print();
		System.out.println("----Cerinta 2: ----");
		Cerinta2 c2 = new Cerinta2(rd.getMonitoredData());
		c2.print();
		c2.writeInFile();
		System.out.println("----Cerinta 3: ----");
		Cerinta3 c3 = new Cerinta3(rd.getMonitoredData());
		c3.print();
		c3.writeInFile();
		System.out.println("----Cerinta 4: ----");
		Cerinta4 c4 = new Cerinta4(rd.getMonitoredData());
		c4.print();
		c4.writeInFile();
		System.out.println("----Cerinta 5: ----");
		Cerinta5 c5 = new Cerinta5(rd.getMonitoredData());
		c5.print();
		c5.writeInFile();
	}
}
